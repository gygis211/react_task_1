import React, { useState } from 'react';
import Number from './number'


function App() {
  const [number, setNumber] = useState()
  const handleClick = (event) => {
    setNumber(event.target.id)
  }
  return (
    <div className="App">
      <h1>История</h1>
      <Number myNumber={number} />
      <div onClick={handleClick}>
        <button id="1">1</button>
        <button id="2">2</button>
        <button id="3">3</button>
        <button id="4">4</button>
        <button id="5">5</button>
      </div>
    </div>
  );
}

export default App;
